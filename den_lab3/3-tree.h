//
//  3-tree.h
//  den_lab3
//
//  Created by WhiteFoXX on 20/05/2019.
//  Copyright © 2019 defaulterrr. All rights reserved.
//

#ifndef __tree_h
#define __tree_h
#include <iostream>
using namespace std;

template <class placeholder>
class Node
{
public:
    placeholder* value = nullptr;
    int holdingAmount=0;
    Node* left = nullptr;
    Node* midlane = nullptr;
    Node* right = nullptr;
    
    Node* operator + (Node const &addedNode)
    {
        Node result;
        result.addAll(this, result);
        result.addAll(addedNode, result);
        return result;
    }
    
    void setObject(placeholder objectToSet)
    {
        value = new placeholder;
        *(value) = objectToSet;
    }
    
    void addObject(placeholder addedObject)         //Adding object
    {
        holdingAmount++;
        if (value == nullptr) {value = new placeholder; *(value) = addedObject;}
        else
        {
            if (*(value) < addedObject) {if (right == nullptr) {right = new Node;right->addObject(addedObject);} else right->addObject(addedObject);
            }
            if (*(value) > addedObject) {if (left == nullptr) {left = new Node;left->addObject(addedObject);} else left->addObject(addedObject);
            }
            if (*(value) == addedObject) {if (midlane == nullptr) {midlane = new Node;midlane->addObject(addedObject);} else midlane->addObject(addedObject);}
        }
        
    }
    
    string outputToString(Node* parent)
    {
        string toReturn = "";
        if (parent->value != nullptr)
        {
            if (parent->right != nullptr) {toReturn+=parent->right->outputToStringElem(right);}
            if (parent->midlane != nullptr) {toReturn+=parent->midlane->outputToStringElem(midlane);}
            if (parent->left != nullptr) {toReturn+=parent->left->outputToStringElem(left);}
            toReturn += *(value);

        }
        return toReturn;
    }
    
    string outputToStringElem(Node* parent)
    {
        string toReturn;
        
        if (parent->value != nullptr)
        {
            if (parent->right != nullptr) {toReturn+=parent->right->outputToStringElem(right);}
            if (parent->midlane != nullptr) {toReturn+=parent->midlane->outputToStringElem(midlane);}
            if (parent->left != nullptr) {toReturn+=parent->left->outputToStringElem(left);}
        }
        toReturn+="|";
        toReturn += *(value);
        toReturn+="|";
        return toReturn;
        
    }
    
    
    void outputFull(Node* parent)
    {
        if (parent->value != nullptr)
        {
            if (parent->left != nullptr) {parent->left->outputFullElem(left);}
            if (parent->midlane != nullptr) {parent->midlane->outputFullElem(midlane);}
            if (parent->right != nullptr) {parent->right->outputFullElem(right);}
            cout << *(parent->value) << endl;
        }
        
    }
    
    int getNodeAmount(Node* parent)
    {
        int height = 0;
        if (parent->value != nullptr)
        {
            height++;
            if (parent->left != nullptr) {height+=parent->left->getNodeAmount(left);}
            if (parent->midlane != nullptr) {height+=parent->midlane->getNodeAmount(midlane);}
            if (parent->right != nullptr) {height+=parent->right->getNodeAmount(right);}
        }
        return height;
    }
    
    void findElementLocation(Node* parent,placeholder objectToFind)
    {
        int buffer;
        cout << "<Root>" << endl;
        
        buffer = findLocation(parent, objectToFind);
        if (buffer == 0) cout << "Object was not found in the tree" << endl;
        if (buffer == 1) cout << "Object was found here" << endl;
    }
    
    void deleteTree(Node* parent)
    {
        if (parent->left != nullptr) {deleteTree(parent->left);delete parent->left;parent->left = nullptr;}
        if (parent->right != nullptr) {deleteTree(parent->right);delete parent->right;parent->right = nullptr;}
        if (parent->midlane != nullptr) {deleteTree(parent->midlane);delete parent->midlane;parent->midlane = nullptr;}
        if (parent->value != nullptr) {delete parent->value;parent->value = nullptr;}
    }
    
    void map_self(Node* parent, placeholder Function(placeholder))
    {
        if (parent->value != nullptr)
        {
            if (parent->right != nullptr) {parent->right->map_self(parent->right, Function);}
            if (parent->midlane != nullptr) {parent->midlane->map_self(parent->right, Function);}
            if (parent->left != nullptr) {parent->left->map_self(parent->right, Function);}
            *(parent->value) = Function(*(parent->value));                                              
        }
    }
    
    Node* map_new(Node* parentTreeNode, placeholder Function(placeholder))
    {
        Node* newTree = new Node;
        parentTreeNode->mapTechInside(parentTreeNode, newTree, Function);
        return newTree;
    }
    
    Node* map_where(Node* parentTreeNode, bool Function(placeholder))
    {
        Node* newTree = new Node;
        parentTreeNode->mapTechInsideWhere(parentTreeNode, newTree, Function);
        return newTree;
    }
    
    void addAll(Node* parentNode, Node* destination)
    {
        if (parentNode->value != nullptr)
        {
            destination->addObject(*(parentNode->value));

            if (parentNode->right != nullptr) {parentNode->right->addAll(parentNode->right, destination);}
            if (parentNode->midlane != nullptr) {parentNode->midlane->addAll(parentNode->right, destination);}
            if (parentNode->left != nullptr) {parentNode->left->addAll(parentNode->right, destination);}
        }
    }
    
    Node* getElement(Node* parent, string path)
    {
        Node* current = parent;
        const char *array = path.c_str();
        bool correct = 1;
        for (int i = 0; i < path.size(); i++)
        {
            if (array[i] == 'l'){
                if(current->left != nullptr)
                    current = current->left;
                else
                {
                    correct = 0;
                    break;
                }
            }
            
            
            else if (array[i] == 'r'){
                if(current->right != nullptr)
                    current = current->right;
                else
                {
                    correct = 0;
                    break;
                }
            }
            
            else if (array[i] == 'm'){
                
                if(current->midlane != nullptr)
                    current = current->midlane;
                else
                {
                    correct = 0;
                    break;
                }
                
                
            }
            else
            {
                correct = 0;
                break;
            }
            
            
        }
        if (correct == 1)
            return (current);
        else
            return nullptr;
        //cout << "Incorrect";
    }
    
    void contaginate(Node* first, Node* second, Node* destination)
    {
        destination->addAll(first, destination);
        destination->addAll(second, destination);
    }
    
    void createByElement(Node* parent, Node* destination, placeholder object)
    {
        if (parent->value != nullptr)
        {
            if (parent->right != nullptr) {parent->right->createByElement(parent->right, destination, object);}
            if (parent->midlane != nullptr) {parent->midlane->createByElement(parent->midlane, destination, object);}
            if (parent->left != nullptr) {parent->left->createByElement(parent->left, destination, object);}
            if (*(parent->value) == object) {destination->addAll(parent, destination);}

        }
    }
    
    bool findSubTree(Node* sourceTree, Node* treeToFind)
    {
        string checker = treeToFind->outputToString(treeToFind);
        Node* buff = sourceTree->findLocationAsNode(sourceTree, *(treeToFind->value));
        if (buff == nullptr) {return false;} else {
        string checker2 = buff->outputToString(buff);
        if (checker == checker2) {return true;}
            else return false;}
    }
    

private:
    
    void outputFullElem(Node* parent)
    {
        if (parent->value != nullptr)
        {
            if (parent->left != nullptr) {parent->left->outputFullElem(left);}
            if (parent->midlane != nullptr) {parent->midlane->outputFullElem(midlane);}
            if (parent->right != nullptr) {parent->right->outputFullElem(right);}
            cout << "|"<<*(parent->value)<<"|";
        }
        
    }
    
    void mapTechInside(Node* parent, Node* destinationTree, placeholder Function(placeholder))
    {
        if (parent->value != nullptr)
        {
            destinationTree->addObject(Function(*(parent->value)));
            if (parent->right != nullptr) {parent->right->mapTechInside(parent->right, destinationTree, Function);}
            if (parent->midlane != nullptr) {parent->midlane->mapTechInside(parent->midlane, destinationTree, Function);}
            if (parent->left != nullptr) {parent->left->mapTechInside(parent->left, destinationTree, Function);}
        }
        
    }
    
    void mapTechInsideWhere(Node* parent, Node* destinationTree, bool Function(placeholder))
    {
        if (parent->value != nullptr)
        {
            if (Function(*(parent->value))) {destinationTree->addObject(*(parent->value));}
            if (parent->right != nullptr) {parent->right->mapTechInsideWhere(parent->right, destinationTree, Function);}
            if (parent->midlane != nullptr) {parent->midlane->mapTechInsideWhere(parent->midlane, destinationTree, Function);}
            if (parent->left != nullptr) {parent->left->mapTechInsideWhere(parent->left, destinationTree, Function);}
        }
        
    }
    
    
    int findLocation(Node* parent, placeholder object)
    {
        if (*(parent->value) != object)
        {
            if (*(value) > object) {if (parent->left != nullptr) {cout << "Left" << endl;return findLocation(parent->left, object);}else return 0;}
            if (*(value) < object) {if (parent->right!= nullptr) {cout << "Right"<< endl;return findLocation(parent->right,object);} else return 0;}
        }
        
        return 1;
    }
    
    Node* findLocationAsNode(Node* parent, placeholder object)
    {
        if (*(parent->value) != object)
        {
            if (*(value) > object) {if (parent->left != nullptr) {return findLocationAsNode(parent->left, object);}else return nullptr;}
            if (*(value) < object) {if (parent->right!= nullptr) {return findLocationAsNode(parent->right,object);} else return nullptr;}
        }
        else
        return parent;
        return nullptr;
    }
    
};


Node<string>* fromStringToTree(string s)
{
    Node<string> (*new_tree) = new Node<string>;
    const string delim = "||";
    const string delim2 = "|";
    string revers = "";
    for(long int i=s.size()-1;i>0;i--)
        revers+=s[i];
    s=revers;
    
    size_t pos=0;
    string token;
    
    if ((pos = s.find(delim2))!=string::npos)
    {
        token = s.substr(0, pos);
        (*new_tree).addObject(token);
        s.erase(0, pos + delim2.length());
    }
    
    while ((pos = s.find(delim))!=string::npos){
        token = s.substr(0, pos);
        (*new_tree).addObject(token);
        s.erase(0, pos + delim.length());
        
    }
    (*new_tree).addObject(s);
    return new_tree;
}






#endif /* __tree_h */

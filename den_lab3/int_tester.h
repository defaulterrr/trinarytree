//
//  int_tester.h
//  den_lab3
//
//  Created by WhiteFoXX on 21/05/2019.
//  Copyright © 2019 defaulterrr. All rights reserved.
//

#ifndef int_tester_h
#define int_tester_h
#include "3-tree.h"
#include "function.h"

void func_tester()
{
    cout << "STARTING FUNCTION TEST" << endl;
    cout << "<<<< Testing tree creation with functions >>>>" << endl;
    Node<int (*)()> l;
    cout << "<<<< Succesfully created tree with function >>>>" << endl;
    int (*f1)() =&inc1;
    int (*f2)() =&inc2;
    int (*f3)() =&inc3;
    int (*f4)() =&inc4;

    l.addObject(f1);
    cout <<"<<<< Pushed f1 to the tree >>>>" << endl;
    
    l.addObject(f2);
    cout <<"<<<< Pushed f2 to the tree >>>>" << endl;

    l.addObject(f3);
    cout <<"<<<< Pushed f3 to the tree >>>>" << endl;

    l.addObject(f4);
    cout <<"<<<< Pushed f4 to the tree >>>>" << endl;

    cout << "<<<< Testing tree output with functions >>>>" << endl;
    l.outputFull(&l);
    cout << "<<<< Succesfully tested tree output with functions >>>>" << endl<< endl;
    cout << (*f2)() << endl;
    cout << endl << endl << endl << endl << endl;
    
    cout << "<<<< Testing another tree and checking if it holds the function >>>>" << endl;
    Node<int (*)()> l2;
    l2.addObject(f2);
    l2.addObject(f1);
    l2.addObject(f3);
    l2.addObject(f4);

    l2.outputFull(&l2);
    Node<int (*)()> *x = l2.getElement(&l2,"m");
    cout << (*(l2.value))() ;
    if (x != nullptr)
        cout << (*(x->value))() << endl << endl;
    cout << "ENDED FUNCTION TEST" <<endl << endl;
}
#endif /* int_tester_h */

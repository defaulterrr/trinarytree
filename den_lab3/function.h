//
//  function.h
//  den_lab3
//
//  Created by WhiteFoXX on 21/05/2019.
//  Copyright © 2019 defaulterrr. All rights reserved.
//

#ifndef function_h
#define function_h

int inc1(){
    return 1;
}
int inc2(){
    return 2;
}
int inc3(){
    return 3;
}
int inc4(){
    return 4;
}

class Intfunc
{
public:
    int (*data)()=nullptr;
    Intfunc(int(*f)()=nullptr){
        data = f;
    }
    friend std::ostream& operator<< (std::ostream &out, const Intfunc &func);
};

std::ostream& operator<< (std::ostream &out, const Intfunc &func)
{
    out << (func.data);
    return out;
}

bool operator>(Intfunc &f1, Intfunc &f2)
{
    return (*(f1.data))() > (*(f2.data))();
}

bool operator<(Intfunc &f1, Intfunc &f2)
{
    return (*(f1.data))() < (*(f2.data))();
}

bool operator==(Intfunc &f1, Intfunc &f2)
{
    return (*(f1.data))() == (*(f2.data))();
}


#endif /* function_h */

//
//  UI.h
//  den_lab3
//
//  Created by WhiteFoXX on 21/05/2019.
//  Copyright © 2019 defaulterrr. All rights reserved.
//

#ifndef UI_h
#define UI_h
#include "3-tree.h"
#include "int_tester.h"
static const char alphanum[] =
"0123456789"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

const int stringLength = sizeof(alphanum) - 1;

char genRandom()
{
    return alphanum[rand() % stringLength];
}

string getString(int n)
{
    string tmp = "";
    char c;
    for (int i=0;i<n;i++)
    {
        c = genRandom();
        tmp.append(&c);
    }
    return tmp;
}


class UI
{
    Node<string> *array;
    int size=0;
    int choice = 0;
    
public:
    void call()
    {
        string options = "1-create new Tree \n2-show all trees\n3-output selected tree\n4-contaginate two trees\n5-create new tree by cutting out the element\n6-check if one tree is a sub-tree for another\n7-check if selected tree has an element\n8-create tree from string\n9-search element by relative path\n10-exit \n\n\nDEV\n11-string tester\n12-func tester(raw)\n";
        cout << options<< endl;
        while (choice!=10)
        {
            
            switch (choice) {
                case 1:
                    createTree();
                    break;
                    
                case 2:
                    outputAllTree();
                    break;
                    
                case 3:
                    outputSelectedTree();
                    break;
                    
                case 4:
                    contaminate();
                    break;
                    
                case 5:
                    createNewByCuttingElement();
                    break;
                    
                case 6:
                    checkForSubTree();
                    break;
                    
                case 7:
                    checkForElementInTree();
                    break;
                    
                case 8:
                    treeFromString();
                    break;
                    
                case 9:
                    relativeSearch();
                    break;
                    
                case 11:
                    string_test();
                    break;
                    
                case 12:
                    func_tester();
                default:
                    break;
            }
            cout << "–––––––––––––––––––" << endl;
            cout << "Main Menu Choice: ";
            cin >> choice;
            cout << "–––––––––––––––––––" << endl;

        }
            
        
    }
    
private:
    
    Node<string> *addTreeToArray()
    {
        size++;
        Node<string>* temp = new Node<string>[size];
        for (int i=0;i<size-1;i++)
        {
            temp[i] = array[i];
        }
        array = temp;
        return &(array[size-1]);
    }
    
    void deleteLastTree()
    {
        Node<string>* temp = new Node<string>[size-1];
        size--;
        for (int i=0;i<size-1;i++)
        {
            temp[i] = array[i];
        }
        array = temp;

    }
    
    
    void createTree()
    {
        string choice;
        cin.ignore(10,'\n');
        cout << "Choose : random or manual" << endl;
        getline(cin,choice);
        
        if(choice == "random")
        {
            Node<string>* manipulated = addTreeToArray();
            for (int i=0;i<10;i++)
            {manipulated->addObject(getString(11));}
        }
        else if (choice == "manual")
        {
        Node<string>* manipulated = addTreeToArray();
        cout << "Enter strings to push to the tree \nWhen you decide to stop enter 0\n";
        string entered;

        while(entered!="0")
        {
            getline(cin,entered);
            if (entered!="0"){
                manipulated->addObject(entered);}
            if (manipulated->getNodeAmount(manipulated) == 0)
            {deleteLastTree();cout << "0 tree cannot be created... Aborting" << endl;};
        }
        }
        
        else {cout << "Aborting, wrong output" << endl;}
    }
    
    void outputAllTree()
    {
        for (int i=0;i<size;i++)
        {
            cout << "Tree #" << i+1 << ": ";
            array[i].outputFull(&array[i]); cout << endl;
        }
    }
    
    void outputSelectedTree()
    {
        cout << "Enter number of the tree" << endl;
        int choice;
        cin >> choice;
        if (choice < 0 || choice > size)
        {
            cout << "Such tree is not present in the system, aborting" << endl;
        }
        else
        {
            array[choice-1].outputFull(&array[choice-1]);cout << endl;
        }
    }
    
    void contaminate()
    {
        if (size<2)
        {
            cout << "Not enough trees are present in the system, maybe you should add some...\nAborting..." << endl;
        }
        else
        {
            cout << "Enter first tree number in the system" << endl;
            int choice1;cin >> choice1;
            if (choice1 < 0 || choice1 > size) {cout << "Such tree is not present in the system, aborting" << endl;return;}
            cout << "Enter second tree number in the system" << endl;
            int choice2;cin >> choice2;
            if (choice2 < 0 || choice2 > size) {cout << "Such tree is not present in the system, aborting" << endl;return;}
            Node<string>* manipulated = addTreeToArray();
            manipulated->contaginate(&array[choice1-1], &array[choice2-1], manipulated);
            cout << endl <<"Resulted tree :";
            cout << manipulated->outputToString(manipulated) << endl;
            
            cout << endl << "Placed under number " << size << " in the array" << endl;
        }
    }
    
    void createNewByCuttingElement()
    {
        cout << "Select tree to work with" << endl;
        int sourceTreeChoice;
        cin >> sourceTreeChoice;
        if (sourceTreeChoice > size || sourceTreeChoice < 0) {cout << "Such tree is missing in the system, aborting..." << endl;return;}
        cout << "Enter an element to find and cut from" << endl;
        string element;
        cin.ignore(10,'\n');
        getline(cin,element);
        
        Node<string> *manipulated = addTreeToArray();
        manipulated->createByElement(&array[sourceTreeChoice-1], manipulated, element);
        
        cout << "Created new tree from the element you specified, keep in mind that empty tree is still a tree :)" << endl;
        manipulated->outputFull(manipulated);
        cout << endl;
        
    }
    
    void checkForSubTree()
    {
        if (size < 2)
        {cout << "Not enough trees are present in the system, consider creating some... \nAborting..." << endl; return;}
        else{
        cout << "Select tree to work with" << endl;
        int sourceTreeChoice1;
        cin >> sourceTreeChoice1;
        if (sourceTreeChoice1 > size || sourceTreeChoice1 < 0) {cout << "Such tree is missing in the system, aborting..." << endl;return;}
        
        cout << "Select tree which is supposedly the sub-tree of the first one" << endl;
        int sourceTreeChoice2;
        cin >> sourceTreeChoice2;
        if (sourceTreeChoice2 > size || sourceTreeChoice2 < 0) {cout << "Such tree is missing in the system, aborting..." << endl;return;}
        
        bool itIs = array[sourceTreeChoice1-1].findSubTree(&array[sourceTreeChoice1-1], &array[sourceTreeChoice2-1]);
        if (itIs) {cout << "Indeed it is the sub-tree" << endl;}
        else {cout << "It is not a sub-tree" << endl;}
            cout << endl;}
    }
    
    void checkForElementInTree()
    {
        if (size < 1)
        {cout << "Not enough trees are present in the system, consider creating some... \nAborting..." << endl; return;}
        else
        {
            cout << "Enter number of the tree to work with" << endl;
            int treeChoice;
            cin >> treeChoice;
            Node<string> manipulated = array[treeChoice-1];
            cout << "Enter the string element to look for in the tree" << endl;
            string objectToFind;
            cin.ignore(10,'\n');
            getline(cin, objectToFind);
            manipulated.findElementLocation(&manipulated, objectToFind);
            
        }
    }
    
    void treeFromString()
    {
        Node<string> *manipulated = addTreeToArray();
        cout << "Enter string to create a tree from. Make sure to keep the form of the string" << endl;
        cin.ignore(10,'\n');
        string toCreateFrom;
        getline(cin,toCreateFrom);
        *(manipulated) = *(fromStringToTree(toCreateFrom));
        
        cout << "Here is the resulted tree" << endl;
        manipulated->outputFull(manipulated);
    }
    
    void relativeSearch()
    {
        if (size < 1)
        {cout << "Not enough trees are present in the system, consider creating some... \nAborting..." << endl; return;}
        else
        {
            cout << "Enter number of the tree to work with" << endl;
            int treeChoice;
            cin >> treeChoice;
            Node<string> manipulated = array[treeChoice-1];
            cout << "Enter the relative path to search in the tree" << endl;
            string pathToLookIn;
            cin.ignore(10,'\n');
            getline(cin, pathToLookIn);
                    Node<string>* potential;
                    potential = manipulated.getElement(&manipulated, pathToLookIn);
                    if (potential == nullptr)
                    {cout << "Such object was not found in the tree" << endl;}
                    else
                    {potential->outputFull(potential);}
                    return;
            
        }
    }
};


#endif /* UI_h */

//
//  string_tester.h
//  den_lab3
//
//  Created by WhiteFoXX on 21/05/2019.
//  Copyright © 2019 defaulterrr. All rights reserved.
//

#ifndef string_tester_h
#define string_tester_h
#include "3-tree.h"

string returnwithnochange(string s)
{
    char buff = s[0];
    int buff2 = int(buff);
    buff2++;
    buff = char(buff2);
    s[0]=buff;
    return s;
}

bool stringcheck(string s)
{
    if (s=="A" || s=="B" || s=="C") {return true;}
    else {return false;}
}

void string_test()
{
    Node<string> string_tree;
    cout << "<<<<<< Testing tree of type String for proper items insertion >>>>>>" << endl;
    string_tree.addObject("A");
    cout << "<Pushed A to the tree>" << endl;
    string_tree.addObject("B");
    cout << "<Pushed B to the tree>" << endl;
    string_tree.addObject("C");
    cout << "<Pushed C to the tree>" << endl;
    string_tree.addObject("D");
    cout << "<Pushed D to the tree>" << endl;
    string_tree.addObject("E");
    cout << "<Pushed E to the tree>" << endl;
    cout << "<<<<<< Succesfully pushed items to the tree>>>>>>" << endl;
    
    
    cout << "<<<<<< Testing tree of type String for proper output functionality >>>>>>" << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    string_tree.outputFull(&string_tree);
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;

    cout << "<<<<<< Succesfully tested tree output >>>>>>" << endl;
    
    cout << endl << endl;

    
    cout << "<<<<<< Testing tree of type String for proper item location finding, if it IS in this tree>>>>>>" << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    string_tree.findElementLocation(&string_tree, "A");
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    cout << "<<<<<< Testing tree of type String for proper item location finding, if it IS NOT in this tree>>>>>>" << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    string_tree.findElementLocation(&string_tree, "a");
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    
    cout << endl << endl;

    
    cout << "<<<<<< Testing tree of type String for proper remapping with CUSTOM function>>>>>>" << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    string_tree.map_self(&string_tree, returnwithnochange);
    string_tree.outputFull(&string_tree);
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    cout << "<<<<<< Succesfully tested tree of type String for proper remapping with CUSTOM function>>>>>>" << endl;
    
    cout << endl << endl;

    
    cout << "<<<<<< Testing tree of type String for proper where-remapping with CUSTOM BOOL function>>>>>>" << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    Node<string> where_test;
    where_test = *(where_test.map_where(&string_tree, stringcheck));
    where_test.outputFull(&where_test);
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    cout << "<<<<<< Succesfully tested tree of type String for proper where-remapping with CUSTOM BOOL function>>>>>>" << endl;
    
    cout << endl << endl;
    
    cout << "<<<<<< Testing tree of type String for proper contamination of two trees>>>>>>" << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    Node<string> contamination_tree;
    contamination_tree.contaginate(&string_tree, &where_test, &contamination_tree);
    contamination_tree.outputFull(&contamination_tree);
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    cout << "<<<<<< Succesfully tested tree of type String for proper contamination of two trees>>>>>>" << endl;
    
    cout << endl << endl;
    
    cout << "<<<<<< Testing tree of type String for proper sub-tree finding (SHOULD BE NEGATIVE)>>>>>>" << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    Node<string> lookForSubTree_test;
    bool found = lookForSubTree_test.findSubTree(&string_tree, &where_test);
    if (found) {cout << "Found tree" << endl; } else {cout << "Didn't find tree" << endl;}
    
    cout << "<<<<<< Testing tree of type String for proper sub-tree finding (SHOULD BE POSITIVE)>>>>>>" << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    
    Node<string> lookForSubTree_test2;
    lookForSubTree_test2.addObject("C");
    lookForSubTree_test2.addObject("D");

    Node<string> lookForSubTree_test3;
    lookForSubTree_test3.addObject("B");
    lookForSubTree_test3.addObject("A");
    lookForSubTree_test3.addObject("C");
    lookForSubTree_test3.addObject("D");

    bool found2 = lookForSubTree_test.findSubTree(&lookForSubTree_test3, &lookForSubTree_test2);
    if (found2) {cout << "Found tree" << endl; } else {cout << "Didn't find tree" << endl;}
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    cout << "<<<<<< Succesfully tested tree of type String for proper sub-tree finding>>>>>>" << endl;
    
    cout << endl << endl;
    
    cout << "<<<<<< Testing tree of type String for proper tree cut-out>>>>>>" << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    cout << "Cutting out node with C root with POSITIVE outcome" << endl;
    Node<string> cutTree;
    cutTree.createByElement(&lookForSubTree_test3,&cutTree,"C");
    cutTree.outputFull(&cutTree);
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    cout << "<<<<<< Succesfully tested tree of type String for proper tree cut-out>>>>>>" << endl;
    
    cout << endl << endl;
    
    cout << "<<<<<< Testing tree of type String for proper tree cut-out>>>>>>" << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    cout << "Cutting out node with E root with NEGATIVE outcome" << endl;
    Node<string> cutTree2;
    cutTree2.createByElement(&lookForSubTree_test3,&cutTree2,"E");
    cutTree2.outputFull(&cutTree2);
    cout << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    cout << "<<<<<< Succesfully tested tree of type String for proper tree cut-out>>>>>>" << endl;
    
    cout << endl << endl;

    cout << "<<<<<< Testing tree of type String for proper return of string path>>>>>>" << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    Node<string> stringTree1;
    string path = string_tree.outputToString(&string_tree);
    cout << path<< endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    cout << "<<<<<< Succesfully tested tree of type String for proper return of string path>>>>>>" << endl;
    
    cout << endl << endl;

    cout << "<<<<<< Testing tree of type String for proper element finding by relative path of RR with POSITIVE result>>>>>>" << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    Node<string> *relativeTree;
    relativeTree = relativeTree->getElement(&string_tree, "rr");
    if (relativeTree == nullptr) {cout << "Not found" << endl;} else {relativeTree->outputFull(relativeTree);};
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    cout << "<<<<<< Succesfully tested tree of type String for proper element finding by relative path>>>>>>" << endl;
    
    cout << endl << endl;
    
    cout << "<<<<<< Testing tree of type String for proper element finding by relative path of LL with NEGATIVE result>>>>>>" << endl;
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    relativeTree = relativeTree->getElement(&string_tree, "l");
    if (relativeTree == nullptr) {cout << "Not found" << endl;} else {relativeTree->outputFull(relativeTree);};
    cout << "–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––" << endl;
    cout << "<<<<<< Succesfully tested tree of type String for proper element finding by relative path>>>>>>" << endl;
    
}



#endif /* string_tester_h */
